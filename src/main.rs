// SPDX-License-Identifier: AGPL-3.0-or-later
// (C) Copyright 2024 Kunal Mehta <legoktm@debian.org>
mod query;

#[macro_use]
extern crate rocket;

use crate::query::Row;
use cached::proc_macro::{cached, once};
use mwbot::Bot;
use mwtitle::namespace::NS_CATEGORY;
use mwtitle::TitleCodec;
use rocket::http::ContentType;
use rocket::State;
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::Serialize;
use std::collections::BTreeSet;
use toolforge::pool::{WikiPool, WEB_CLUSTER};

/// The context needed to render the "index.html" template
#[derive(Serialize)]
struct IndexTemplate {
    wikis: BTreeSet<String>,
}

#[derive(Serialize)]
struct ErrorTemplate {
    code: u16,
    reason: &'static str,
    error: Option<String>,
}

#[derive(Responder)]
#[response(status = 500, content_type = "text/html")]
struct Error(Template);

impl From<anyhow::Error> for Error {
    fn from(value: anyhow::Error) -> Self {
        Self(Template::render(
            "error",
            ErrorTemplate {
                code: 500,
                reason: "Internal Server Error",
                error: Some(value.to_string()),
            },
        ))
    }
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[once(sync_writes = true, result = true, time = 86400)]
async fn all_wikis(pool: &WikiPool) -> anyhow::Result<BTreeSet<String>> {
    Ok(pool.list_domains().await?)
}

#[cached(sync_writes = true, result = true, time = 604800)]
async fn bot(domain: String) -> anyhow::Result<Bot> {
    Ok(Bot::builder(format!("https://{domain}/w/")).build().await?)
}

/// Handle all GET requests for the "/" route. We return either a `Template`
/// instance, or a `Template` instance with a specific HTTP status code.
///
/// We ask Rocket to give us the managed State (see <https://rocket.rs/v0.5-rc/guide/state/#managed-state>)
/// of the database connection pool we set up below.
#[get("/")]
async fn index(pool: &State<WikiPool>) -> Result<Template> {
    let wikis = all_wikis(pool).await?;
    Ok(Template::render("index", IndexTemplate { wikis }))
}

#[get("/lookup?<category>&<domain>")]
async fn lookup_route(
    pool: &State<WikiPool>,
    category: String,
    domain: String,
) -> Result<Template> {
    Ok(lookup(pool, category, domain).await?)
}

#[derive(Serialize)]
struct LookupTemplate {
    category: String,
    rows: Vec<Row>,
    domain: String,
    wikis: BTreeSet<String>,
}

async fn lookup(
    pool: &WikiPool,
    category: String,
    domain: String,
) -> anyhow::Result<Template> {
    let mut conn = pool.connect_by_domain(&domain).await?;
    // At this point we've validated the domain is legit
    let bot = bot(domain.to_string()).await?;
    let category = normalize_category(bot.title_codec(), category);
    let rows = query::lookup(&mut conn, &category).await?;
    let wikis = all_wikis(pool).await?;
    Ok(Template::render(
        "lookup",
        LookupTemplate {
            category,
            domain,
            rows,
            wikis,
        },
    ))
}

fn normalize_category(codec: &TitleCodec, category: String) -> String {
    match codec.new_title_with_namespace(&category, NS_CATEGORY) {
        Ok(title) => title.dbkey().to_string(),
        Err(_) => category,
    }
}

#[get("/static/styles.css")]
fn styles() -> (ContentType, &'static str) {
    (ContentType::CSS, include_str!("../static/styles.css"))
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        // Create a new database connection pool. We intentionally do not use
        // Rocket's connection pooling facilities as Toolforge policy requires that
        // we do not hold connections open while not in use. The Toolforge crate builds
        // a connection string that configures connection pooling in a way that doesn't
        // leave unused idle connections hanging around.
        .manage(WikiPool::new(WEB_CLUSTER).expect("unable to load db config"))
        .mount("/", routes![index, lookup_route, styles])
        .attach(Template::fairing())
        .attach(Healthz::fairing())
}
