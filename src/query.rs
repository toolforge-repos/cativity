// SPDX-License-Identifier: AGPL-3.0-or-later
// (C) Copyright 2024 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use serde::Serialize;
use std::collections::BTreeSet;
use toolforge::pool::mysql_async::{prelude::*, Conn};

#[derive(Serialize)]
pub(crate) struct Row {
    user_name: String,
    ug_groups: BTreeSet<String>,
    user_editcount: u64,
    rev_timestamp: Option<String>,
}

const QUERY: &str = r#"
select
  user_name,
  group_concat(ug_group separator ','),
  user_editcount,
  rev_timestamp
from
  page
join categorylinks
  on page_id = cl_from
join user
  on user_name = replace(page_title, '_', ' ')
left join user_groups
  on user_id = ug_user
join actor
  on user_id = actor_user
left join revision_userindex
  on rev_id =(
    select
      rev_id
    from
      revision_userindex
    where
      rev_actor = actor_id
    order by
      rev_timestamp desc
    limit
      1
  )
where
  page_namespace in (2, 3)
  and cl_to = ?
group by
  user_name;
"#;

pub(crate) async fn lookup(
    conn: &mut Conn,
    category: &str,
) -> Result<Vec<Row>> {
    let mut rows = conn
        .exec_map(
            QUERY,
            (category,),
            |(user_name, ug_groups, user_editcount, rev_timestamp): (
                String,
                Option<String>,
                u64,
                Option<String>,
            )| {
                let ug_groups = ug_groups
                    .map(|val| val.split(',').map(|v| v.to_string()).collect())
                    .unwrap_or_default();
                Row {
                    user_name,
                    ug_groups,
                    user_editcount,
                    rev_timestamp,
                }
            },
        )
        .await?;
    rows.sort_by_cached_key(|r| {
        r.rev_timestamp.as_deref().unwrap_or("0").to_string()
    });
    rows.reverse();
    Ok(rows)
}
